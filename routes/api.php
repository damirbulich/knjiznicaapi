<?php

use App\Http\Controllers\SearchController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get("borrow", "BorrowController@index");
Route::get("borrow/{id}", "BorrowController@show");
Route::post("borrow/return", "BorrowController@giveBack");
Route::post("borrow", "BorrowController@store");
Route::get("user/{id}/books", "BorrowController@forUser");

Route::get("user", "UserController@index");
Route::get("user/{id}", "UserController@show");
Route::post("user/{id}", "UserController@update");
Route::post("user", "UserController@store");

Route::get("book", "BookController@index");
Route::get("book/{id}", "BookController@show");
Route::get("book/title/{oznaka}", "BookController@list");
Route::post("book", "BookController@store");

Route::get("author", "AuthorController@index");
Route::get("author/{id}", "AuthorController@show");
Route::get("author/find/{name}", "AuthorController@list");
Route::post("author", "AuthorController@store");

Route::get("title", "TitleController@index");
Route::get("title/{id}", "TitleController@show");
Route::get("title/find/{title}", "TitleController@list");
Route::post("title", "TitleController@store");

Route::get("library", "LibraryController@index");
Route::post("library", "LibraryController@store");

Route::get("librarian", "LibrarianController@index");
Route::get("librarian/{id}", "LibrarianController@show");
Route::post("librarian", "LibrarianController@store");

Route::get("search", "SearchController@pretrazi");

Route::fallback(function () {
    return response()->json([
        "message" => "Page not found!"
    ]);
});
