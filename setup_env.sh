#!bin/bash
echo APP_NAME=$APPNAME >> .env
echo APP_ENV=local >> .env
echo APP_DEBUG=true >> .env
echo APP_URL=http://localhost >> .env
echo LOG_CHANNEL=stack >> .env
echo DB_CONNECTION=mysql >> .env
echo DB_HOST=127.0.0.1 >> .env
echo DB_PORT=3306 >> .env
echo DB_DATABASE=$DATABASE >> .env
echo DB_USERNAME=$USERNAME >> .env
echo DB_PASSWORD=$PASSWORD >> .env
