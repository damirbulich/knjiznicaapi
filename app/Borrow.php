<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Borrow extends Model
{
    protected $guarded = [];

    public function book()
    {
        return $this->belongsTo(Book::class, "book_id", "id");
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function librarian()
    {
        return $this->belongsTo('App\Librarian', 'librarian_id', 'id');
    }
}