<?php

namespace App\Http\Controllers;

use App\Http\Resources\TitleResource;
use App\Title;
use Illuminate\Http\Request;

class TitleController extends Controller
{
    public function index()
    {
        $naslovi = Title::all();

        return TitleResource::collection($naslovi);
    }

    public function show($oznaka)
    {
        $naslov = Title::where("id", $oznaka)->get();

        return TitleResource::collection($naslov);
    }

    public function list($title)
    {
        $naslovi = Title::whereRaw("LOWER(`naslov`) LIKE ?", ["%" . trim(strtolower($title)) . "%"])->get();

        return TitleResource::collection($naslovi);
    }

    public function store()
    {
        $data = request()->validate([
            "id" => "required|unique:titles,id",
            "naslov" => "required",
            "author_id" => "required"
        ]);

        $naslov = Title::create($data);

        return new TitleResource($naslov);
    }
}