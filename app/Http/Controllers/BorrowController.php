<?php

namespace App\Http\Controllers;

use App\Borrow;
use App\Http\Resources\BorrowResource;
use Carbon\Carbon;
use Illuminate\Http\Request;

class BorrowController extends Controller
{

    public function index(Request $request)
    {
        $knjiznica = $request->knjiznica_id;
        //dd($knjiznica);
        $posudbe = Borrow::whereIn("book_id", function ($query) use ($knjiznica) {
            $query->select("id")
                ->from("books")
                ->where("library_id", "=", $knjiznica)
                ->get();
        })->get();
        //dd($posudbe);

        return BorrowResource::collection($posudbe);
    }

    public function store()
    {
        $data = request()->validate([
            "book_id" => "required",
            "user_id" => "required",
            "uzeo" => "required",
            "librarian_id" => "required",
        ]);

        $created = Borrow::create($data);

        return new BorrowResource($created);
    }

    public function forUser($id)
    {
        $data = Borrow::where("user_id", $id)->get();

        return BorrowResource::collection($data);
    }

    public function giveBack(Request $request)
    {
        $request->validate([
            "id" => "required",
        ]);

        Borrow::where("id", $request->id)
            ->update(["vratio" => Carbon::now()]);
        $posudba = Borrow::where("id", $request->id)->first();

        return new BorrowResource($posudba);
    }
}
