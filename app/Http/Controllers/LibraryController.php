<?php

namespace App\Http\Controllers;

use App\Http\Resources\LibraryResource;
use App\Library;
use Illuminate\Http\Request;

class LibraryController extends Controller
{
    public function index()
    {
        $knjiznice = Library::all();

        return LibraryResource::collection($knjiznice);
    }

    public function store()
    {
        $data = request()->validate([
            "naziv" => "required"
        ]);

        $knjiznica = Library::create($data);

        return new LibraryResource($knjiznica);
    }
}