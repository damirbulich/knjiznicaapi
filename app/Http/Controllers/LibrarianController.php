<?php

namespace App\Http\Controllers;

use App\Http\Resources\LibrarianResource;
use App\Http\Resources\LibraryResource;
use App\Librarian;
use Illuminate\Http\Request;

class LibrarianController extends Controller
{
    public function index()
    {
        $knjiznicari = Librarian::all();

        return LibrarianResource::collection($knjiznicari);
    }

    public function show($id)
    {
        $knjiznicar = Librarian::where("id", $id)->get();

        return LibrarianResource::collection($knjiznicar);
    }

    public function store()
    {
        $data = request()->validate([
            "ime" => "required",
            "prezime" => "required"
        ]);

        $knjiznicar = Librarian::create($data);

        return new LibrarianResource($knjiznicar);
    }
}