<?php

namespace App\Http\Controllers;

use App\Author;
use App\Http\Resources\AuthorResource;
use Illuminate\Http\Request;

class AuthorController extends Controller
{
    public function index()
    {
        $autori = Author::all();

        return AuthorResource::collection($autori);
    }

    public function show($id)
    {
        $autor = Author::where("id", $id)->get();

        return AuthorResource::collection($autor);
    }

    public function list($name)
    {
        $autori = Author::whereRaw("LOWER(`ime`) LIKE ?", ["%" . trim(strtolower($name)) . "%"])
            ->orWhereRaw("LOWER(`prezime`) LIKE ?", ["%" . trim(strtolower($name)) . "%"])
            ->get();

        return AuthorResource::collection($autori);
    }

    public function store()
    {
        $data = request()->validate([
            "ime" => "required",
            "prezime" => "required"
        ]);

        $autor = Author::create($data);

        return new AuthorResource($autor);
    }
}