<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserResource;
use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    public function index()
    {
        $users = User::all();

        return UserResource::collection($users);
    }

    public function show($id)
    {
        $user = User::where("id", $id)->get();

        return UserResource::collection($user);
    }

    public function update($id)
    {
        $data = request()->validate([
            "ime" => "required",
            "prezime" => "required",
            "br_mob" => "required",
            "adresa" => "required",
            "clan_do" => "required"
        ]);

        User::where("id", $id)->update($data);

        $user = User::where("id", $id)->get();

        return UserResource::collection($user);
    }

    public function store()
    {
        $data = request()->validate([
            "ime" => "required",
            "prezime" => "required",
            "br_mob" => "required",
            "adresa" => "required",
            "clan_do" => "required"
        ]);

        $user = User::create($data);

        return new UserResource($user);
    }
}