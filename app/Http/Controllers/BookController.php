<?php

namespace App\Http\Controllers;

use App\Book;
use App\Http\Resources\BookResource;
use Illuminate\Http\Request;

class BookController extends Controller
{
    public function index()
    {
        $books = Book::all();

        return BookResource::collection($books);
    }

    public function list($oznaka)
    {
        $books = Book::where("title_oznaka", $oznaka)->get();

        return BookResource::collection($books);
    }

    public function show($id)
    {
        $book = Book::where("id", $id)->get();

        return BookResource::collection($book);
    }

    public function store()
    {

        $data = request()->validate([
            "title_oznaka" => "required",
            "library_id" => "required"
        ]);

        $book = Book::create($data);

        return new BookResource($book);
    }
}