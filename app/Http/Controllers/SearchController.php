<?php

namespace App\Http\Controllers;

use App\Http\Resources\BookResource;
use App\Book;
use App\Borrow;
use App\Http\Resources\BorrowResource;
use App\Http\Resources\UserResource;
use App\User;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function pretrazi(Request $request){
        if($request->table && $request->data != "" ){
            if($request->table == "book"){
                $knjige = Book::whereRaw("LOWER(`id`) LIKE ?","%".trim(strtolower($request->data))."%")
                ->orWhereRaw("LOWER(`title_oznaka`) LIKE ?","%".trim(strtolower($request->data))."%")
                ->get();
                return BookResource::collection($knjige);
            }
            if ($request->table == "user"){
                $korisnici = User::whereRaw("LOWER(`id`) LIKE ?","%".trim(strtolower($request->data))."%")
                ->orWhereRaw("LOWER(`ime`) LIKE ?","%".trim(strtolower($request->data))."%")
                ->orWhereRaw("LOWER(`prezime`) LIKE ?","%".trim(strtolower($request->data))."%")
                ->get();
                return UserResource::collection($korisnici);
            }
            if ($request->table == "borrow"){
                $posudbe = Borrow::whereRaw("LOWER(`id`) LIKE ?","%".trim(strtolower($request->data))."%")
                ->orWhereRaw("LOWER(`user_id`) LIKE ?","%".trim(strtolower($request->data))."%")
                ->orWhereRaw("LOWER(`book_id`) LIKE ?","%".trim(strtolower($request->data))."%")
                ->get();
                return BorrowResource::collection($posudbe);
            }
        }
        return UserResource::collection([]);
    }
}
