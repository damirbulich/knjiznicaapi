<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Book;
use App\Librarian;
use App\User;

class BorrowResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "book" => new BookResource(Book::find($this->book_id)),
            "user" => new UserResource(User::find($this->user_id)),
            "uzeo" => $this->uzeo,
            "vratio" => $this->vratio,
            "librarian" => new LibrarianResource(Librarian::find($this->librarian_id)),
        ];
    }
}