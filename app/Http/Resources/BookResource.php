<?php

namespace App\Http\Resources;

use App\Library;
use App\Title;
use Illuminate\Http\Resources\Json\JsonResource;

class BookResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "title" => new TitleResource(Title::find($this->title_oznaka)),
            "library" => new LibraryResource(Library::find($this->library_id)),
        ];
    }
}