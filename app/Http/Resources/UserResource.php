<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "ime" => $this->ime,
            "prezime" => $this->prezime,
            "br_mob" => $this->br_mob,
            "adresa" => $this->adresa,
            "clan_do" => $this->clan_do,
        ];
    }
}