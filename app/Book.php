<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $guarded = [];

    public function title()
    {
        return $this->belongsTo('App\Title', 'title_oznaka', 'id');
    }

    public function library()
    {
        return $this->belongsTo('App\Library', 'library_id', 'id');
    }
}