<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Title extends Model
{
    protected $guarded = [];

    public $incrementing = false;

    public function author()
    {
        return $this->belongsTo('App\Author', 'author_id', 'id');
    }
}